//
//  testSideMenu2App.swift
//  testSideMenu2
//
//  Created by miaowang on 2022/9/7.
//

import SwiftUI

@main
struct testSideMenu2App: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
