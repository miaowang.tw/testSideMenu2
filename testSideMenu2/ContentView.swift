//
//  ContentView.swift
//  testSideMenu2
//
//  Created by miaowang on 2022/9/7.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
